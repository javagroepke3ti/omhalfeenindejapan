/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.javaopdracht1.student;

import java.io.FileInputStream;
import java.util.Collection;

/**
 *
 * @author Thomas
 */
public interface ExcelReader {
    public Collection<Student> readStudents(FileInputStream file);
}
